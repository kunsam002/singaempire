from singaempire import redis, logger
from singaempire.services.site_services import *
import json
from singaempire.core.utils import DateJSONEncoder


class Struct(dict):
    """ Temporary struct class """

    def __init__(self, **kwargs):
        self.update(kwargs)

    def __getattr__(self, k):
        return self.get(k)

    def __setattr__(self, k, v):
        self[k] = v

    def convert_fields(self):
        """ Convert the field from the native python format based on the elasticsearch index mapping """

        __tablename__ = getattr(self, "__tablename__", None)

        if __tablename__:
            for k, v in self.__dict__.items():

                cls_ = get_model_from_table_name(__tablename__)

                if cls_:
                    _field = cls_.__table__.columns.get(k)

                    if _field is not None and _field.type.__class__ in [DateTime, Date] and v is not None:
                        setattr(self, str(k), dateutil.parser.parse(v))
                else:
                    setattr(self, str(k), v)

    def as_dict(self):
        _dict = {}

        for key, obj in self.__dict__.items():

            if isinstance(obj, Struct):
                _dict[key] = obj.__dict__

            elif isinstance(obj, (list, tuple)):
                _dict[key] = [s.__dict__ for s in obj]
            else:
                _dict[key] = obj

        return _dict



#loading all states

redis.delete('state')

states = fetch_states()

for state in states:
	redis.zadd('state', states.index(state), json.dumps(state))


#loading all countries

redis.delete('country')

countries = fetch_countries()

for country in countries:
	redis.zadd('country', countries.index(country), json.dumps(country))


#loading all artists

redis.delete('artist')

artists = fetch_artists()

for artist in artists:
	redis.zadd('artist', artists.index(artist), json.dumps(artist))

#loading all albums

redis.delete('album')

albums = fetch_albums()

for album in albums:
	redis.zadd('album', albums.index(album), json.dumps(album))

#loading all songs

redis.delete('song')

songs = fetch_songs()

for song in songs:
	redis.zadd('song', songs.index(song), json.dumps(song))


#loading all news

redis.delete('news')

news = fetch_news()

for post in news:
	redis.zadd('news', news.index(post), json.dumps(post))


#loading recent messages

redis.delete('recent_message')

messages = fetch_recent_messages()

for message in messages:
	redis.zadd('recent_message', messages.index(message), json.dumps(message))


def load_states():
	return [json.loads(state, object_hook=lambda x: Struct(**x)) for state in redis.zrange('state', 0, -1)]

def load_countries():
	return [json.loads(country, object_hook=lambda x: Struct(**x)) for country in redis.zrange('country', 0, -1)]

def load_artists():
	return [json.loads(artist, object_hook=lambda x: Struct(**x)) for artist in redis.zrange('artist', 0, -1)]

def load_albums():
	return [json.loads(album, object_hook=lambda x: Struct(**x)) for album in redis.zrange('album', 0, -1)]

def load_songs():
	return [json.loads(song, object_hook=lambda x: Struct(**x)) for song in redis.zrange('song', 0, -1)]

def load_recent_messages():
	return [json.loads(message, object_hook=lambda x: Struct(**x)) for message in redis.zrange('recent_message', 0, -1)]


def load_news():
	return [json.loads(news, object_hook=lambda x: Struct(**x)) for news in redis.zrange('news', 0, -1)]

