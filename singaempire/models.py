# Models

from singaempire import db, bcrypt, app, logger
from singaempire.core.utils import slugify, id_generator
from flask.ext.login import UserMixin
from sqlalchemy import or_
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method, Comparator
from sqlalchemy.inspection import inspect
from datetime import datetime
import hashlib
from sqlalchemy import func, asc, desc
from sqlalchemy import inspect, UniqueConstraint, desc
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy.orm import dynamic
from flask import url_for
import inspect as pyinspect
import time


# SQLAlchemy Continuum
# import sqlalchemy as sa
# from sqlalchemy_continuum.ext.flask import FlaskVersioningManager
# from sqlalchemy_continuum import make_versioned

# make_versioned(manager=FlaskVersioningManager())


def get_model_from_table_name(tablename):
    """ return the Model class for a given __tablename__ """

    _models = [args[1] for args in globals().items() if pyinspect.isclass(args[
                                                                              1]) and issubclass(args[1], db.Model)]

    for _m in _models:
        try:
            if _m.__tablename__ == tablename:
                return _m
        except Exception, e:
            logger.info(e)
            raise

    return None


def slugify_from_name(context):
    """
    An sqlalchemy processor that works with default and onupdate
    field parameters to automatically slugify the name parameters in the model
    """
    return slugify(context.current_parameters['name'])


def generate_token_code(context):
    return hashlib.md5("%s:%s:%s" % (
        context.current_parameters["shop_id"], context.current_parameters["email"], str(datetime.now()))).hexdigest()


def generate_user_token_code(context):
    return hashlib.md5("%s:%s:%s" % (
        context.current_parameters["user_id"], context.current_parameters["email"], str(datetime.now()))).hexdigest()


class AppMixin(object):
    """ Mixin class for general attributes and functions """

    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    # __versioned__ = {} # SQLAlchemy Continuum

    @declared_attr
    def is_deleted(cls):
        return db.Column(db.Boolean, default=False)

    @declared_attr
    def date_created(cls):
        return db.Column(db.DateTime, default=datetime.utcnow)

    @declared_attr
    def last_updated(cls):
        return db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def as_full_dict(self):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        return dict([(k, getattr(self, k)) for k in data.attrs.keys()])

    def as_dict(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[]):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        if include_only is None:
            include_only = data.attrs.keys() + extras

        else:
            include_only = include_only + extras + ["id", "last_updated", "date_created"]

        _dict = dict([(k, getattr(self, k)) for k in include_only if isinstance(getattr(self, k),
                                                                                (hybrid_property, InstrumentedAttribute,
                                                                                 InstrumentedList,
                                                                                 dynamic.AppenderMixin)) is False and k not in exclude])

        for key, obj in _dict.items():
            is_list_int = False
            if isinstance(obj, db.Model):
                _dict[key] = obj.as_dict()

            if isinstance(obj, datetime):
                # _dict[key] = unicode(obj).replace(" ", "T")
                _dict[key] = time.mktime(obj.timetuple())

            if isinstance(obj, (list, tuple)):
                items = []
                print key, obj, "the obj"
                for item in obj:
                    print item, "the item"
                    if type(item) in [int, str]:
                        items.append(item)
                        is_list_int = True
                    else:
                        inspect_item = inspect(item)
                        items.append(
                            dict([(k, getattr(item, k)) for k in inspect_item.attrs.keys() + extras if
                                  k not in exclude and hasattr(item, k)]))
                if is_list_int:
                    _dict[key] = items
                    continue
                for item in items:
                    obj = item.get(child)
                    if obj:
                        item[child] = obj.as_dict(extras=child_include)
        return _dict

    def as_dict_inner(self, include_only=None, exclude=["is_deleted"], extras=[], child=None, child_include=[]):
        """ Retrieve all values of this model as a dictionary """
        data = inspect(self)

        if include_only is None:
            include_only = data.attrs.keys() + extras

        else:
            include_only = include_only + extras

        _dict = dict([(k, getattr(self, k)) for k in include_only if isinstance(getattr(self, k),
                                                                                (hybrid_property, InstrumentedAttribute,
                                                                                 InstrumentedList,
                                                                                 dynamic.AppenderMixin)) is False and k not in exclude])

        for key, obj in _dict.items():
            if isinstance(obj, db.Model):
                _dict[key] = obj.as_dict()

            if isinstance(obj, (list, tuple)):
                items = []
                for item in obj:
                    inspect_item = inspect(item)
                    items.append(
                        dict([(k, getattr(item, k)) for k in inspect_item.attrs.keys() + extras if
                              k not in exclude and hasattr(item, k)]))

                for item in items:
                    obj = item.get(child)
                    if obj:
                        item[child] = obj.as_dict(extras=child_include)
        return _dict


class PlatformMixin(AppMixin):
    """ Mixin class for Shop related attributes and functions """

    @declared_attr
    def platform_id(cls):
        return db.Column(db.Integer, db.ForeignKey('platform.id'), nullable=False)


class UserMixin(AppMixin, UserMixin):
    """ Mixin class for User related attributes and functions """

    @declared_attr
    def user_id(cls):
        return db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    @property
    def user(self):
        if self.user_id:
            return User.query.get(self.user_id)
        else:
            return None


class ArtistMixin(AppMixin):
    """ Mixin class for Artist related attributes and functions """

    @declared_attr
    def artist_id(cls):
        return db.Column(db.Integer, db.ForeignKey('artist.id'), nullable=True)

    @property
    def artist(self):
        if self.artist_id:
            return Artist.query.get(self.artist_id)
        else:
            return None


class City(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    code = db.Column(db.String(200))
    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=False)
    state = db.relationship("State", foreign_keys="City.state_code")
    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=False)
    country = db.relationship("Country", foreign_keys="City.country_code")


class State(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=False, index=True, unique=True, primary_key=True)
    name = db.Column(db.String(200))
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=False)
    country = db.relationship("Country", foreign_keys="State.country_code")


class Country(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=False, index=True, unique=True, primary_key=True)
    name = db.Column(db.String(200))
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)
    phone_code = db.Column(db.String(200))
    enabled = db.Column(db.Boolean, default=False)


class Timezone(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    code = db.Column(db.String(200))
    offset = db.Column(db.String(200))  # UTC time


class Genre(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=False, index=True, unique=True, primary_key=True)
    name = db.Column(db.String(200))
    slug = db.Column(db.String(200), nullable=False, default=slugify_from_name)

    @property
    def songs(self):
        return Song.query.filter(Song.genre_code == self.code).all()

    @property
    def total_songs(self):
        return Song.query.filter(Song.genre_code == self.code).count()


class User(db.Model, UserMixin, AppMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), unique=True)
    email = db.Column(db.String(200), unique=True)
    full_name = db.Column(db.String(200), unique=False)
    password = db.Column(db.Text, unique=False)
    active = db.Column(db.Boolean, default=False)
    is_confirmed = db.Column(db.Boolean, default=False)
    is_staff = db.Column(db.Boolean, default=False)
    is_global = db.Column(db.Boolean, default=False)
    is_super_admin = db.Column(db.Boolean, default=False)
    deactivate = db.Column(db.Boolean, default=False)
    gender = db.Column(db.String(200))
    location = db.Column(db.String(200))
    dob = db.Column(db.DateTime)

    # roles = db.relationship('Role', secondary="user_roles",
    #   backref = db.backref('users', lazy = 'dynamic'))

    @hybrid_property
    def roles(self):
        """ Fetch user roles as a list of roles """
        roles = []
        for access_group in self.access_groups:
            for role in access_group.roles:
                roles.append(role)

        return list(set(roles))

    def __repr__(self):
        return '<User %r>' % self.username

    def get_auth_token(self):
        """ Returns the user's authentication token """
        return hashlib.md5("%s:%s" % (self.username, self.password)).hexdigest()

    def is_authenticated(self):
        """ For login manager """
        return True

    def is_active(self):
        """ Returns if the user is active or not. Overriden from UserMixin """
        return self.active

    def generate_password(self, password):
        """
        Generates a password from the plain string

        :param password: plain password string
        """

        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        """
        Checks the given password against the saved password

        :param password: password to check
        :type password: string

        :returns True or False
        :rtype: bool

        """
        return bcrypt.check_password_hash(self.password, password)

    def set_password(self, new_password):
        """
        Sets a new password for the user

        :param new_password: the new password
        :type new_password: string
        """

        self.generate_password(new_password)
        db.session.add(self)
        db.session.commit()

    def add_role(self, role):
        """
        Adds a the specified role to the user
        :param role: Role ID or Role name or Role object to be added to the user

        """
        try:

            if isinstance(role, Role) and hasattr(role, "id"):
                role_obj = role
            elif type(role) is int:
                role_obj = Role.query.get(role)
            elif type(role) is str:
                role_obj = Role.query.filter(Role.name == role.lower()).one()

            if not role_obj:
                raise Exception("Specified role could not be found")

            if self.has_role(role_obj) is False:
                self.roles.append(role_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except:
            db.session.rollback()
            raise

    def has_role(self, role):
        """
        Returns true if a user identifies with a specific role

        :param role: A role name or `Role` instance
        """
        return role in self.roles

    def add_access_group(self, access_group):
        """
        Adds a the specified access group to the user
        :param group: AccessGroup ID or AccessGroup name or AccessGroup object to be added to the user

        """
        try:

            if isinstance(access_group, AccessGroup) and hasattr(access_group, "id"):
                group_obj = access_group
            elif type(access_group) is int:
                group_obj = AccessGroup.query.get(access_group)
            elif type(access_group) is str:
                group_obj = AccessGroup.query.filter(
                    AccessGroup.name == access_group.lower()).one()

            if not group_obj:
                raise Exception("Specified access_group could not be found")

            if self.has_access_group(group_obj) is False:
                self.access_groups.append(group_obj)
                db.session.add(self)
                db.session.commit()
                return self
        except:
            db.session.rollback()
            raise

    def has_access_group(self, access_group):
        """
        Returns true if a user identifies with a specific access_group

        :param access_group: A access_group name or `AccessGroup` instance
        """
        return access_group in self.access_groups


class Platform(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False, nullable=False)
    handle = db.Column(db.String(200), unique=True, nullable=False)
    description = db.Column(db.Text)
    about_information = db.Column(db.Text)  # information for your about page
    thumbnail = db.Column(db.Text)  # storefront thumbnail url
    logo = db.Column(db.Text)  # storefront thumbnail url
    email = db.Column(db.String(200), nullable=False, unique=True)
    phone = db.Column(db.String(200))
    street = db.Column(db.String(200))
    city = db.Column(db.String(200))
    is_enabled = db.Column(db.Boolean, default=False)
    twitter_handle = db.Column(db.String(200))
    facebook_handle = db.Column(db.String(200))
    google_handle = db.Column(db.String(200))
    instagram_handle = db.Column(db.String(200))
    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=True)
    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=True)
    timezone_id = db.Column(
        db.Integer, db.ForeignKey('timezone.id'), nullable=True)
    contents = db.relationship(
        'Content', backref='platform', lazy='dynamic', cascade="all,delete-orphan")


class Content(PlatformMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    description = db.Column(db.Text, unique=False)
    title = db.Column(db.String(200), unique=False)
    caption = db.Column(db.Text, unique=False, default=0)
    sku = db.Column(db.String(200), unique=False)
    url = db.Column(db.String(200), unique=False)
    is_featured = db.Column(db.Boolean, default=True)
    visibility = db.Column(db.Boolean, default=False)
    cover_image_id = db.Column(db.Integer)  # over image among all images
    is_private = db.Column(db.Boolean, default=False)


class Image(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    alt_text = db.Column(db.String(200), unique=False)
    url = db.Column(db.String(200), unique=False)
    width = db.Column(db.Float)
    height = db.Column(db.Float)
    artist_id = db.Column(
        db.Integer, db.ForeignKey('artist.id'), nullable=True)
    album_id = db.Column(
        db.Integer, db.ForeignKey('album.id'), nullable=True)
    song_id = db.Column(
        db.Integer, db.ForeignKey('song.id'), nullable=True)
    gallery_id = db.Column(
        db.Integer, db.ForeignKey('gallery.id'), nullable=True)
    event_id = db.Column(
        db.Integer, db.ForeignKey('event.id'), nullable=True)
    post_id = db.Column(
        db.Integer, db.ForeignKey('post.id'), nullable=True)

    def delete_file(self):
        """ Deletes the actual image file from disk """
        raise NotImplementedError()

    @property
    def cover_image(self):
        return self

    def __repr__(self):
        return '<Image %r>' % self.name


class Artist(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)  # stage name
    real_name = db.Column(db.String(200), unique=False)
    gender = db.Column(db.String(200))
    dob = db.Column(db.DateTime)
    occupation = db.Column(db.String(200), unique=False)
    state_code = db.Column(db.String(200), db.ForeignKey('state.code'), nullable=False)
    state = db.relationship("State", foreign_keys="Artist.state_code")
    country_code = db.Column(db.String(200), db.ForeignKey('country.code'), nullable=False)
    country = db.relationship("Country", foreign_keys="Artist.country_code")
    twitter_handle = db.Column(db.String(200))
    facebook_handle = db.Column(db.String(200))
    google_handle = db.Column(db.String(200))
    instagram_handle = db.Column(db.String(200))
    linkedin_handle = db.Column(db.String(200))
    cover_image_id = db.Column(db.Integer)  # over image among all images
    description = db.Column(db.Text, unique=False)
    images = db.relationship('Image', backref='artist',
                             lazy='dynamic', cascade="all,delete-orphan")
    artist_songs = db.relationship(
        'Song', secondary="artist_contributed_songs", backref=db.backref('artist', lazy='dynamic'))

    @property
    def cover_image(self):
        """ Retrieves the cover image from the list of images """
        if self.cover_image_id:
            return Image.query.get(self.cover_image_id)
        else:
            cover_image = self.images.filter().first()
            if cover_image:
                self.cover_image_id = cover_image.id

            # On first call, set the cover image id to prevent subsequent
            # searching
            try:
                db.session.add(self)
                db.session.commit()
            except:
                db.session.rollback()
                raise

            return cover_image

    @property
    def cover_image_url(self):
        return self.cover_image.url

    @property
    def songs(self):
        return Song.query.filter(Song.artist_id == self.id).all()

    @property
    def total_songs(self):
        return Song.query.filter(Song.artist_id == self.id).count()

    @property
    def total_albums(self):
        return Album.query.filter(Album.artist_id == self.id).count()


class Album(ArtistMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)  # stage name
    cover_image_id = db.Column(db.Integer)  # over image among all images
    description = db.Column(db.Text, unique=False)
    images = db.relationship('Image', backref='album',
                             lazy='dynamic', cascade="all,delete-orphan")
    songs = db.relationship('Song', backref='album',
                            lazy='dynamic', cascade="all")
    release_date = db.Column(db.DateTime)

    @property
    def cover_image(self):
        """ Retrieves the cover image from the list of images """
        if self.cover_image_id:
            return Image.query.get(self.cover_image_id)
        else:
            cover_image = self.images.filter().first()
            if cover_image:
                self.cover_image_id = cover_image.id

            # On first call, set the cover image id to prevent subsequent
            # searching
            try:
                db.session.add(self)
                db.session.commit()
            except:
                db.session.rollback()
                raise

            return cover_image

    @property
    def cover_image_url(self):
        return self.cover_image.url if self.cover_image else None

    @property
    def total_songs(self):
        return Song.query.filter(Song.album_id == self.id).count()


# Association table for Artist and Song
artist_contributed_songs = db.Table('artist_contributed_songs',
                                    db.Column('artist_id', db.Integer,
                                              db.ForeignKey('artist.id')),
                                    db.Column('song_id', db.Integer,
                                              db.ForeignKey('song.id'))
                                    )


class Song(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), unique=False)
    album_id = db.Column(db.Integer, db.ForeignKey('album.id'), nullable=True)
    artist_id = db.Column(db.Integer, nullable=False)
    contributing_artists = db.relationship(
        'Artist', secondary="artist_contributed_songs", backref=db.backref('song', lazy='dynamic'))
    track_url = db.Column(db.Text, unique=False)
    cover_image_id = db.Column(db.Integer)  # over image among all images
    genre_code = db.Column(db.String(200), db.ForeignKey('genre.code'), nullable=False)
    genre = db.relationship("Genre", foreign_keys="Song.genre_code")
    release_date = db.Column(db.DateTime)

    @property
    def artist(self):
        return Artist.query.get(self.artist_id)

    @property
    def cover_image(self):
        """ Retrieves the cover image from the list of images """
        if self.cover_image_id:
            return Image.query.get(self.cover_image_id)

    @property
    def cover_image_url(self):
        return self.cover_image.url if self.cover_image else None


class News(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), unique=False)
    cover_image_id = db.Column(db.Integer)  # over image among all images
    info = db.Column(db.Text, unique=False)


class Event(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)  # stage name
    cover_image_id = db.Column(db.Integer)  # over image among all images
    description = db.Column(db.Text, unique=False)
    event_date = db.Column(db.DateTime)

    @property
    def cover_image(self):
        """ Retrieves the cover image from the list of images """
        if self.cover_image_id:
            return Image.query.get(self.cover_image_id)

    @property
    def cover_image_url(self):
        return self.cover_image.url if self.cover_image else None


class Post(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)  # stage name
    cover_image_id = db.Column(db.Integer)  # over image among all images
    info = db.Column(db.Text, unique=False)

    @property
    def cover_image(self):
        """ Retrieves the cover image from the list of images """
        if self.cover_image_id:
            return Image.query.get(self.cover_image_id)

    @property
    def cover_image_url(self):
        return self.cover_image.url if self.cover_image else None


class AdminMessage(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    phone = db.Column(db.String(200), nullable=False)
    subject = db.Column(db.Text)
    is_read = db.Column(db.Boolean, default=False)
    user_read = db.Column(db.Boolean, default=False)
    has_parent = db.Column(db.Boolean, default=False)
    is_replied = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    date_replied = db.Column(db.DateTime, nullable=True)
    body = db.Column(db.Text)  # for plain text messages
    responses = db.relationship(
        'AdminMessageResponse', backref='admin_message', lazy='dynamic', cascade="all,delete-orphan")


class AdminMessageResponse(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    admin_message_id = db.Column(db.Integer, db.ForeignKey(
        'admin_message.id'), nullable=False)
    body = db.Column(db.Text)  # for plain text messages
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)


class Notification(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    email = db.Column(db.Text, nullable=True)
    sms = db.Column(db.Text, nullable=True)
    push = db.Column(db.Text, nullable=True)
    code = db.Column(db.String(200), nullable=False, unique=True)

    # relationship to compiled email
    emails = db.relationship('Email', backref='notification', lazy='dynamic', cascade='all,delete-orphan')
    # relationship to compiled sms
    sms_ = db.relationship('Sms', backref='notification', lazy='dynamic', cascade='all,delete-orphan')


class Email(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(400), nullable=False)
    html = db.Column(db.Text)
    text = db.Column(db.Text)
    send_at = db.Column(db.DateTime)
    sent_at = db.Column(db.DateTime)
    status = db.Column(db.String(200))
    message_id = db.Column(db.String(200))
    to = db.Column(db.String(200), nullable=False)
    from_ = db.Column(db.String(200), nullable=False)
    notification_id = db.Column(db.Integer, db.ForeignKey('notification.id'), nullable=False)


class Sms(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text)
    to = db.Column(db.String(200), nullable=False)
    from_ = db.Column(db.String(200), nullable=False)
    send_at = db.Column(db.DateTime)
    status = db.Column(db.String(200), default='pending')
    message_id = db.Column(db.String(200))
    notification_id = db.Column(db.Integer, db.ForeignKey('notification.id'), nullable=False)
    sent_at = db.Column(db.DateTime)


class ContentType(AppMixin, db.Model):
    code = db.Column(db.String(200), nullable=False, index=True, unique=True, primary_key=True)
    name = db.Column(db.String(200))


class Gallery(AppMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False)
    image_id = db.Column(db.Integer, db.ForeignKey('image.id'), nullable=True)
    video = db.Column(db.Text)  # for plain text messages
    content_type_code = db.Column(db.String(200), db.ForeignKey('content_type.code'), nullable=False)
    country = db.relationship("ContentType", foreign_keys="Gallery.content_type_code")
