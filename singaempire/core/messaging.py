__author__ = 'kunsam002'

from singaempire import login_manager, app, logger, mail
from singaempire.models import *
import json
import requests
import urllib
from flask_mail import Message
from flask import make_response


def send_email(body="", sender=[], recipients=[], cc=None, bcc=None, subject="", text="", html=None, files=None, channel=None):

    msg = Message(subject, sender=("Singa Empire", "info@singaempire.com"), recipients=recipients)

    msg.body = body
    msg.html = html
    try:
        mail.send(msg)

        res = {"success": True}
        res = json.dumps(res)
        response = make_response(res)
        return response
    except:
        res = {"success": False}
        res = json.dumps(res)
        response = make_response(res)
        # return response
        raise


def send_push_notification(**kwargs):
    url = app.config.get("FIREBASE_PUSH_NOTI_URL")
    header_auth_key= app.config.get("FIREBASE_AUTH_HEADER_KEY")
    headers= {"content-type":"application/json","Authorization":header_auth_key}
    data = json.dumps(kwargs.get("data"))
    try:
        resp=requests.post(url, data=data, headers=headers)
    except:
        pass
