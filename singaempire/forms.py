__author__ = 'kunsam002'

from flask_wtf import Form
from wtforms import Field, TextField, PasswordField, StringField, FieldList, FormField, \
    DateTimeField, DateField, BooleanField, DecimalField, validators, HiddenField, FloatField, \
    IntegerField, TextAreaField, SelectField, RadioField, SelectMultipleField, FileField
from wtforms.validators import DataRequired, Optional, Email, EqualTo, ValidationError
from datetime import datetime, date
from flask_wtf.html5 import EmailField
from wtforms import widgets


class LoginForm(Form):
    username = StringField('Username or Email Address', validators=[DataRequired()],
                           description="Please enter a registered username or email")
    password = PasswordField('Password', validators=[DataRequired()], description="Please enter your valid password")
    remember = BooleanField('Remember', validators=[Optional()])

class SignupForm(Form):
    full_name = StringField('Your Full Name', validators=[DataRequired()])
    username = StringField('Username or Email Address', validators=[DataRequired()],
                           description="Please enter a registered username or email")
    password = PasswordField('Password', validators=[DataRequired()])
    verify_password = PasswordField('Verify Password', validators=[DataRequired(), EqualTo('password')])
    email = EmailField('Email Address', validators=[DataRequired(), Email()])
    gender = SelectField('Gender', coerce=str, validators=[DataRequired()],
                         choices=[("Male", "Male"), ("Female", "Female")])

class NewsletterSubscriberForm(Form):
    email = EmailField('Email Address', validators=[DataRequired(), Email()])


class CountryForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])


class StateForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])
    country_id = SelectField('Country', coerce=int, validators=[DataRequired()])


class CityForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    url = StringField('Url', validators=[Optional()])

class ContactForm(Form):
    name = StringField('Full Name', validators=[DataRequired()])
    subject = StringField('Subject', validators=[DataRequired()])
    email = StringField('Email Address', validators=[DataRequired(), Email()])
    phone = StringField('Phone Number', validators=[DataRequired()])
    body = StringField('Message', validators=[DataRequired()], widget=widgets.TextArea())



class ArtistForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    real_name = StringField('Real Name', validators=[DataRequired()])
    dob = DateField("Date of Birth", validators=[DataRequired()])
    occupation = StringField('Occupation', validators=[DataRequired()])
    gender = SelectField('Gender', coerce=str, validators=[DataRequired()],
                         choices=[("Male", "Male"), ("Female", "Female")])
    state_code = SelectField('State', coerce=str, validators=[DataRequired()])
    country_code = SelectField('Country', coerce=str, validators=[DataRequired()])
    twitter_handle = StringField('Twitter Handle', validators=[Optional()])
    facebook_handle = StringField('Facebook Handle', validators=[Optional()])
    google_handle = StringField('Google Handle', validators=[Optional()])
    instagram_handle = StringField('Instagram Handle', validators=[Optional()])
    linkedin_handle = StringField('LinkedIn Handle', validators=[Optional()])
    description = StringField('Description', validators=[DataRequired()], widget=widgets.TextArea(),
                              description="A detailed description on the Artist.")


class UpdateArtistForm(ArtistForm):
    cover_image_id = IntegerField('Cover Image', widget=widgets.HiddenInput(), validators=[DataRequired()])


class AlbumForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()], widget=widgets.TextArea(),
                              description="A detailed description on the Album.")
    artist_id = SelectField('Artist', coerce=int, validators=[DataRequired()])
    release_date = DateField("Release Date", validators=[DataRequired()])

class UpdateAlbumForm(AlbumForm):
    cover_image_id = IntegerField('Cover Image', widget=widgets.HiddenInput(), validators=[DataRequired()])

class GenreForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])

class ContentTypeForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    code = StringField('Code', validators=[DataRequired()])


class NewsForm(Form):
    title = StringField('Title', validators=[DataRequired()])
    info = StringField('Information', validators=[DataRequired()], widget=widgets.TextArea(),
                              description="A detailed description on the News.")


class GalleryForm(Form):
    name = StringField('Name', validators=[Optional()])
    content_type_code = SelectField('ContentType', coerce=str, validators=[DataRequired()])
    video = StringField('Video', validators=[Optional()], widget=widgets.TextArea(),
                              description="Video iframe")
    is_gallery = BooleanField('Gallery', validators=[Optional()], default=False)

class SongForm(Form):
    title = StringField('Title', validators=[DataRequired()])
    album_id = SelectField('Album', coerce=int, validators=[Optional()])
    artist_id = SelectField('Artist', coerce=int, validators=[DataRequired()])
    track_url = StringField('Track URL', validators=[DataRequired()])
    genre_code = SelectField('Genre', coerce=str, validators=[DataRequired()])
    release_date = DateField("Release Date", validators=[DataRequired()])

class EventForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    description = StringField('Description',widget=widgets.TextArea(), validators=[DataRequired()])
    event_date = DateField("Event Date", validators=[DataRequired()])
class PostForm(Form):
    name = StringField('Name', validators=[DataRequired()])
    info = StringField('Description',widget=widgets.TextArea(), validators=[DataRequired()])
