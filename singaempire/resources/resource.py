from singaempire.resources import BaseResource, ModelListField, ModelField
from singaempire.services.accounts import *
from singaempire.forms import *
from singaempire import register_api
from flask_restful import fields
from singaempire import logger


class UserResource(BaseResource):

    resource_name = 'users'
    service_class = UserService
    validation_form = SignupForm
    resource_fields = {
        "username": fields.String,
        "full_name": fields.String
    }




register_api(UserResource, '/users/', '/users/<int:id>/', '/users/<string>/')