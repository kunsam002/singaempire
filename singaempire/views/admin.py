"""
admin.py

@Author: Ogunmokun, Olukunle

The admin views required to sign up and get started
"""

from flask import Blueprint, render_template, abort, redirect, \
    flash, url_for, request, session, g, make_response, current_app
from flask_login import logout_user, login_required, login_user, current_user
from singaempire import db, logger, app, handle_uploaded_photos, redis_worker, login_manager, redis
from singaempire.forms import *
from singaempire.services import *
from singaempire.services import accounts, assets, posts, site_services
from datetime import date, datetime, timedelta
from singaempire.models import *
from sqlalchemy import asc, desc, or_, and_, func
from singaempire.forms import *
from singaempire.core import utils, templating
from singaempire.core.utils import build_page_url
from singaempire.signals import *
import time
import json
import urllib
from flask.ext.principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
import base64
import requests
import xmltodict
import os
import sys
import random
import pprint
import cgi

control = Blueprint('control', __name__)


@app.errorhandler(400)
def page_not_found(e):
    title = "400- PAGE NOT FOUND!"
    today = date.today()
    return render_template('admin/errors/page-error-400.html', **locals()), 400


@app.errorhandler(403)
def forbidden_error(e):
    title = "403- FORBIDDEN ERROR!"
    today = date.today()
    return render_template('admin/errors/page-error-403.html', **locals()), 403


@app.errorhandler(404)
def page_unfound(e):
    title = "404- PAGE NOT FOUND!"
    today = date.today()
    return render_template('admin/errors/page-error-404.html', **locals()), 404


@app.errorhandler(500)
def page_not_found(e):
    title = "500- INTERNAL SERVER ERROR!"
    today = date.today()
    return render_template('admin/errors/page-error-500.html', **locals()), 500


@app.errorhandler(503)
def internal_server_error(e):
    title = "503- GETTING UP"
    today = date.today()
    return render_template('admin/errors/page-error-503.html', **locals()), 503


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.before_request
def pre_load_user():
    user_id = session.get("current_user_id", None)
    if user_id:
        user = load_user(user_id)
        g.user = user


def object_length(data):
    return len(data)


def _timestamp_to_date(stamp):
    try:
        return datetime.fromtimestamp(stamp)
    except:
        return None


@control.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = date.today()
    minimum = min
    string = str
    number_format = utils.number_format
    length = object_length
    join_list = utils.join_list
    slugify = utils.slugify
    paging_url_build = build_page_url
    clean_ascii = utils.clean_ascii
    recent_messages = redis_worker.load_recent_messages()
    timestamp_to_date = _timestamp_to_date
    tot_artists = length(redis_worker.load_artists())
    tot_albums = length(redis_worker.load_albums())
    tot_songs = length(redis_worker.load_songs())
    tot_news = News.query.count()
    return locals()


# login page
@control.route('/login/', methods=["GET", "POST"])
def login():
    next_url = request.args.get("next") or url_for(".index")
    if request.method == 'GET':
        session['next_url'] = next_url
    r_code = request.args.get("code", None)
    action = request.args.get("action", None)

    form = LoginForm()
    if form.validate_on_submit():
        data = form.data
        username = data["username"]
        password = data["password"]
        remember = data.get("remember", True)
        user = accounts.authenticate_user(username, password)

        if user is not None:
            login_user(user, remember=True, force=True)  # This is necessary to remember the user

            # include user roles into scope
            # identity_changed.send(app, identity=Identity(user.id))
            # next_url = request.args.get("next", url_for(".index"))
            next_url = session.get('next_url', url_for(".index"))
            logger.info(next_url)
            resp = redirect(next_url)
            # Transfer auth token to the frontend for use with api requests
            __xcred = base64.b64encode("%s:%s" % (user.email, user.get_auth_token()))

            # return set_cookies(resp, user_id=user.id, remember=remember, credentials=__xcred)
            resp.set_cookie("__xcred", __xcred)
            session["current_user_id"] = user.id
            session.permanent = True

            return resp
        else:
            flash("The username or password is invalid", "login")
            return redirect(url_for('.login'))

    return render_template("admin/login.html", **locals())


@control.route('/logout/')
@login_required
def logout():
    # if not current_user.is_authenticated():
    #     return redirect(url_for('.signup'))
    logout_user()
    session.pop("current_user_id")
    resp = redirect(url_for('.login'))
    resp.set_cookie("__xcred", '', expires=0)
    return resp


@control.route('/')
@login_required
def index():
    return render_template('admin/index.html', **locals())


@control.route('/users/')
@login_required
def users():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = User.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/accounts/users.html', **locals())


@control.route('/users/create/', methods=["GET", "POST"])
@login_required
def create_user():
    form = SignupForm()

    if form.validate_on_submit():
        data = form.data

        obj = accounts.UserService.create(**data)
        if obj:
            flash("User Updated successfully!")
            return redirect(url_for('.users'))
    return render_template('admin/accounts/create_user.html', **locals())


@control.route('/users/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_user(id):
    obj = User.query.get(id)
    if not obj:
        abort(404)
    form = SignupForm(obj=obj)

    if form.validate_on_submit():
        data = form.data

        obj = accounts.UserService.update(obj.id, **data)
        if obj:
            flash("User Updated successfully!")
            return redirect(url_for('.users'))
    return render_template('admin/accounts/create_user.html', **locals())


@control.route('/messages/inbox/')
@control.route('/messages/')
@login_required
def messages():
    return render_template('admin/communications/messages.html', **locals())


@control.route('/messages/<int:id>/')
@login_required
def view_message(id):
    obj = AdminMessage.query.get(id)
    if not obj:
        abort(404)
    responses = AdminMessageResponse.query.filter(AdminMessageResponse.admin_message_id == obj.id).order_by(
        AdminMessageResponse.id).all()
    return render_template('admin/communications/view_message.html', **locals())


@control.route('/artists/')
@login_required
def artists():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Artist.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/assets/artists.html', **locals())


@control.route('/artists/create/', methods=["GET", "POST"])
@login_required
def create_artist():
    form = ArtistForm()
    form.state_code.choices = [("", "--- Select One ---")] + [(i.code, i.name) for i in redis_worker.load_states()]
    form.country_code.choices = [("", "--- Select One ---")] + [(i.code, i.name) for i in redis_worker.load_countries()]

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.ArtistService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(artist_id=obj.id, **upload_)

            if obj:
                flash("Artist created successfully!")
                redis.zadd('artist', obj.id, json.dumps(obj.as_dict(extras=["state","country","total_songs","total_albums","cover_image_url"])))
                return redirect(url_for('.artists'))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/assets/create_artist.html', **locals())


@control.route('/artists/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_artist(id):
    obj = Artist.query.get(id)
    if not obj:
        abort(404)
    obj.cover_image
    form = UpdateArtistForm(obj=obj, data={"cover_image_id": obj.cover_image_id})
    form.state_code.choices = [("", "--- Select One ---")] + [(i.code, i.name) for i in redis_worker.load_states()]
    form.country_code.choices = [("", "--- Select One ---")] + [(i.code, i.name) for i in redis_worker.load_countries()]

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    existing_images = obj.images.all()

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.ArtistService.update(obj.id, **data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(artist_id=obj.id, **upload_)

            if obj:
                flash("Artist updated successfully!")
                return redirect(url_for('.artists'))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/assets/create_artist.html', **locals())


@control.route('/albums/')
@login_required
def albums():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Album.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/assets/albums.html', **locals())


@control.route('/albums/create/', methods=["GET", "POST"])
@login_required
def create_album():
    form = AlbumForm()
    form.artist_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in redis_worker.load_artists()]
    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.AlbumService.create(**data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(album_id=obj.id, **upload_)

            if obj:
                flash("Album created successfully!")
                redis.zadd('album', obj.id, json.dumps(obj.as_dict(extras=["total_songs"])))

                return redirect(url_for('.albums'))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/assets/create_album.html', **locals())


@control.route('/albums/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_album(id):
    obj = Album.query.get(id)
    if not obj:
        abort(404)
    obj.cover_image
    form = UpdateAlbumForm(obj=obj, data={"cover_image_id": obj.cover_image_id})
    form.artist_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in redis_worker.load_artists()]
    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    existing_images = obj.images.all()

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.AlbumService.update(obj.id, **data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(album_id=obj.id, **upload_)

            if obj:
                flash("Album created successfully!")
                return redirect(url_for('.albums'))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/assets/create_album.html', **locals())


@control.route('/songs/')
@login_required
def songs():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Song.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/assets/songs.html', **locals())


@control.route('/songs/create/', methods=["GET", "POST"])
@login_required
def create_song():
    form = SongForm()
    form.artist_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in redis_worker.load_artists()]
    form.album_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in redis_worker.load_albums()]
    form.genre_code.choices = [("", "--- Select One ---")] + [(i.code, i.name) for i in Genre.query.all()]
    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit():
        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        data=form.data
        obj = assets.SongService.create(**data)
        if obj:

            if uploaded_files:
                images = []
                for upload_ in uploaded_files:
                    images = assets.SongService.upload_image(obj.id, **upload_)

            flash("Song created successfully!")
            return redirect(url_for('.songs'))

    return render_template('admin/assets/create_song.html', **locals())


@control.route('/songs/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_song(id):
    obj = Song.query.get(id)
    if not obj:
        abort(404)
    form = SongForm(obj=obj, data={"cover_image_id": obj.cover_image_id})
    form.artist_id.choices = [(0, "--- Select One ---")] + [(i.id, i.name) for i in redis_worker.load_artists()]
    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    existing_images = obj.images.all()

    if form.validate_on_submit() and empty_files is False:

        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data
            obj = assets.SongService.update(obj.id, **data)

            images = []
            for upload_ in uploaded_files:
                images = assets.ImageService.create(album_id=obj.id, **upload_)

            if obj:
                flash("Song updated successfully!")
                return redirect(url_for('.songs'))

        else:
            flash("Error: Image not found!")
            form.errors["images"] = errors

    return render_template('admin/assets/create_song.html', **locals())


@control.route('/genres/')
@login_required
def genres():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Genre.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/assets/genres.html', **locals())


@control.route('/genres/create/', methods=["GET", "POST"])
@login_required
def create_genre():
    form = GenreForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.GenreService.create(**data)

        if obj:
            flash("Genre created successfully!")
            return redirect(url_for('.genres'))

    return render_template('admin/assets/create_genre.html', **locals())


@control.route('/genres/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_genre(code):
    obj = Genre.query.get(code)
    if not obj:
        abort(404)
    form = GenreForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.GenreService.update(obj.code, **data)

        if obj:
            flash("Genre updated successfully!")
            return redirect(url_for('.genres'))

    return render_template('admin/assets/create_genre.html', **locals())


@control.route('/content_types/')
@login_required
def content_types():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = ContentType.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/assets/content_types.html', **locals())


@control.route('/content_types/create/', methods=["GET", "POST"])
@login_required
def create_content_type():
    form = ContentTypeForm()

    if form.validate_on_submit():

        data = form.data
        obj = assets.ContentTypeService.create(**data)

        if obj:
            flash("Content Type created successfully!")
            return redirect(url_for('.content_types'))

    return render_template('admin/assets/create_content_types.html', **locals())


@control.route('/content_types/<string:code>/update/', methods=["GET", "POST"])
@login_required
def update_content_type(code):
    obj = ContentType.query.get(code)
    if not obj:
        abort(404)
    form = ContentTypeForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = assets.ContentTypeService.update(obj.code, **data)

        if obj:
            flash("Content Type updated successfully!")
            return redirect(url_for('.content_types'))

    return render_template('admin/assets/create_content_types.html', **locals())

@control.route('/gallery/')
@login_required
def gallery():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Gallery.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/assets/gallery.html', **locals())


@control.route('/gallery/create/', methods=["GET", "POST"])
@login_required
def create_gallery():
    form = GalleryForm()
    form.content_type_code.choices=[("","--- Select One ---")]+[(i.code,i.name) for i in ContentType.query.all()]

    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit():
        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)
        data = form.data
        obj = assets.GalleryService.create(**data)


        if uploaded_files:
            data = form.data

            images = []
            for upload_ in uploaded_files:
                images = assets.GalleryService.upload_image(obj.id, **upload_)


        if obj:
            flash("Gallery created successfully!")
            return redirect(url_for('.gallery'))

    return render_template('admin/assets/create_gallery.html', **locals())


@control.route('/components/')
@login_required
def components():
    return render_template('admin/components.html', **locals())


@control.route('/news/')
@login_required
def news():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = News.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
    return render_template('admin/posts/news.html', **locals())


@control.route('/news/create/', methods=["GET", "POST"])
@login_required
def create_news():
    form = NewsForm()

    if form.validate_on_submit():

        data = form.data
        data["user_id"] = current_user.id
        obj = posts.NewsService.create(**data)

        if obj:
            flash("News created successfully!")
            return redirect(url_for('.news'))

    return render_template('admin/posts/create_news.html', **locals())


@control.route('/news/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_news(id):
    obj = News.query.get(id)
    if not obj:
        abort(404)
    form = NewsForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = posts.NewsService.update(obj.id, **data)

        if obj:
            flash("News updated successfully!")
            return redirect(url_for('.news'))

    return render_template('admin/posts/create_news.html', **locals())


@control.route('/events/')
@login_required
def events():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Event.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('admin/posts/events.html', **locals())

@control.route('/events/create/', methods=["GET", "POST"])
@login_required
def create_events():
    form = EventForm()
    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit():

        data = form.data
        data["user_id"] = current_user.id
        obj = posts.EventService.create(**data)


        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data

            images = []
            for upload_ in uploaded_files:
                images = posts.EventService.upload_image(**upload_)

        if obj:
            flash("Event created successfully!")
            return redirect(url_for('.events'))

    return render_template('admin/posts/create_events.html', **locals())


@control.route('/events/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_events(id):
    obj = Event.query.get(id)
    if not obj:
        abort(404)
    form = EventForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = posts.EventService.update(obj.id, **data)

        if obj:
            flash("Event updated successfully!")
            return redirect(url_for('.events'))

    return render_template('admin/posts/create_events.html', **locals())


@control.route('/posts/')
@login_required
def published_posts():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Post.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
    return render_template('admin/posts/posts.html', **locals())


@control.route('/posts/create/', methods=["GET", "POST"])
@login_required
def create_posts():
    form = PostForm()
    files = request.files.getlist("images")
    # Test for the first entry
    empty_files = False
    for _f in files:
        if _f.filename == '' or utils.check_extension(_f.filename) is False:
            empty_files = True
            break

    if form.validate_on_submit():

        data = form.data
        data["user_id"] = current_user.id
        obj = posts.PostService.create(**data)



        uploaded_files, errors = handle_uploaded_photos(files, (160, 160), square=False)

        if uploaded_files:
            data = form.data

            images = []
            for upload_ in uploaded_files:
                images = posts.PostService.upload_image(**upload_)

        if obj:
            flash("Post created successfully!")
            return redirect(url_for('.published_posts'))
    return render_template('admin/posts/create_posts.html', **locals())


@control.route('/posts/<int:id>/update/', methods=["GET", "POST"])
@login_required
def update_posts(id):
    obj = Post.query.get(id)
    if not obj:
        abort(404)
    form = PostForm(obj=obj)

    if form.validate_on_submit():

        data = form.data
        obj = posts.PostService.update(obj.id, **data)

        if obj:
            flash("Post updated successfully!")
            return redirect(url_for('.published_posts'))

    return render_template('admin/posts/create_posts.html', **locals())


