"""
public.py

@Author: Ogunmokun, Olukunle

The public views required to sign up and get started
"""

from flask import Blueprint, render_template, abort, redirect, \
    flash, url_for, request, session, g, make_response, current_app, jsonify
from flask.ext.login import logout_user, login_required, login_user, current_user
from singaempire import db, logger, app, redis_worker
from singaempire.forms import *
from singaempire.services import *
from singaempire.services import accounts, assets
from datetime import date, datetime, timedelta
from singaempire.models import *
from sqlalchemy import asc, desc, or_, and_, func
from singaempire.forms import *
from singaempire.core import utils, templating
from singaempire.core.utils import build_page_url
from singaempire.signals import *
import time
import json
import urllib
from flask.ext.principal import Principal, Identity, AnonymousIdentity, identity_changed, PermissionDenied
import base64
import requests
import xmltodict
import os
import sys
import random
import pprint
import cgi

www = Blueprint('public', __name__, static_folder='../static')

@app.errorhandler(404)
def page_not_found(e):
    today = date.today()
    title = "404- Ooops! Page Not Found"
    error_number = "404"
    error_title = "Page not found!"
    error_info = "The requested URL was not found on this server. Make sure that the Web site address displayed in the address bar of your browser is spelled and formatted correctly."

    return render_template('public/error.html', **locals()), 404


@app.errorhandler(500)
def internal_server_error(e):
    today = date.today()
    title = "500- Internal Server Error"
    error_number = "500"
    error_title = "Server Error!"
    error_info = "There has been an Internal server Error. Please try again later or Contact the Administrator."

    return render_template('public/error.html', **locals()), 500

@app.login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.before_request
def pre_load_user():
    user_id = session.get("current_user_id",None)
    if user_id:
        user = load_user(user_id)
        g.user = user

def object_length(data):
    return len(data)



def _split_flash(msg):
    logger.info(msg)
    logger.info(msg.split("|"))
    return msg.split("|")

def _timestamp_to_date(stamp):
    try:
        return datetime.fromtimestamp(stamp)
    except:
        return None
def prepare_group_news():
    raw = redis_worker.load_news()

    data ={}
    found_dates = []
    for i in raw:
        i_date = _timestamp_to_date(i.date_created).strftime("%B %Y")
        if i_date in found_dates:
            data[i_date].append(i)
        else:
            found_dates.append(i_date)
            data[i_date] = [i]

    return data

@www.context_processor
def main_context():
    """ Include some basic assets in the startup page """
    today = date.today()
    minimum = min
    string = str
    number_format = utils.number_format
    length = object_length
    join_list = utils.join_list
    slugify = utils.slugify
    paging_url_build = build_page_url
    clean_ascii = utils.clean_ascii
    timestamp_to_date=_timestamp_to_date

    ua = detect_user_agent(request.user_agent.string)
    if ua.is_bot:
        return "NO BOTS ALLOWED"

    return locals()


# @www.route('/<string:path>/')
@www.route('/')
def index():
    events = Event.query.limit(4).all()
    posts = Post.query.limit(3).all()
    return render_template('public/index.html', **locals())


@www.route('/contact-us/')
def contact_us():
    page_title = "Contact Us"
    form = ContactForm()

    if current_user:
        form = ContactForm(obj=current_user)

    if form.validate_on_submit():
        data = form.data
        if current_user:
            data["user_id"] = current_user.id
        else:
            ref_user = User.query.filter(User.email == data.get("email", "")).first()
            if ref_user:
                data["user_id"] = ref_user.id

        obj = assets.AdminMessageService.create(**data)

        flash("Your message has been sent! Thank you".upper())
        return redirect(url_for(".contact_us"))
    return render_template('public/contact.html', **locals())

@www.route('/contact/sending/', methods=['GET', 'POST'])
def contact_sending():
    page_title = "Contact Us"
    if request.method == "POST":
        _data = {}
        if request.data:
            _data = request.data
            _data = json.loads(_data)
        elif request.form:
            _data = json.dumps(request.form)
            _data = json.loads(_data)
        logger.info(_data)
        contact_form = ContactForm(obj=_data)
        if contact_form.validate_on_submit():
            data = contact_form.data
            u = User.query.filter(User.email == data.get("email")).first()
            if u:
                data["user_id"] = u.id
            msg = assets.AdminMessageService.create(**data)

            _data = {"status": "success", "message": "Message Sent successfully"}
            data = jsonify(_data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response
        else:
            logger.info(contact_form.errors)
            _data = {"status": "failure", "message": "Form Validation failed"}
            data = jsonify(_data)
            response = make_response(data)
            response.headers['Content-Type'] = "application/json"
            return response

    else:
        data = {"Status": "failure", "message": "Request Method not Allowed"}
        data = jsonify(data)
        response = make_response(data)
        response.headers['Content-Type'] = "application/json"
        return response


@www.route('/albums/')
def albums():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Album.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
    return render_template('public/albums.html', **locals())


@www.route('/albums/<int:id>/info/')
def album_info(id):
    obj=Album.query.get(id)
    if not obj:
        abort(404)

    songs = Song.query.filter(Song.album_id==obj.id).all()
    return render_template('public/album-info.html', **locals())


@www.route('/artists/')
def artists():

    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = Artist.query

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))

    return render_template('public/artists.html', **locals())


@www.route('/artists/name/')
def artist_info():

    return render_template('public/about-artist.html', **locals())


@www.route('/gallery/')
def gallery():
    return render_template('public/gallery.html', **locals())


@www.route('/news/')
def news():
    try:
        page = int(request.args.get("page", 1))
        pages = request.args.get("pages")
    except:
        abort(404)

    request_args = utils.copy_dict(request.args, {})

    query = News.query


    redis_news=prepare_group_news()

    results = query.paginate(page, 20, False)
    if results.has_next:
        # build next page query parameters
        request_args["page"] = results.next_num
        results.next_page = "%s%s" % ("?", urllib.urlencode(request_args))

    if results.has_prev:
        # build previous page query parameters
        request_args["page"] = results.prev_num
        results.previous_page = "%s%s" % ("?", urllib.urlencode(request.args))
    return render_template('public/news.html', **locals())
