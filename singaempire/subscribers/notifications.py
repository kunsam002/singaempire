__author__ = 'kunsam002'
"""
All subscriptions having to do with accounts services.
"""

from singaempire.signals import *
from singaempire import app, logger
from singaempire.models import *
from singaempire.core.messaging import send_email
from singaempire.core import templating, utils
from datetime import date, datetime, timedelta
from singaempire.services.accounts import UserService
import json


@contact_message.connect
def _contact_message(obj_id, **kwargs):
    """ Sends out an email when the an student signs up """
    today = date.today()

    obj = AdminMessage.query.get(obj_id)
    if not obj:
        raise Exception("Message does not exist")


    html, text = templating.generate_email_content("contact_message", data=locals())

    try:
        send_email(subject="Welcome", recipients=[obj.email,"kunsam002@gmail.com"], html=html, text=text, files=None)
    except:
        _data_ = {"status":"incomplete","message":"Email Notification Sending Failed."}
        return _data_

