__author__ = 'kunsam002'

"""
restful.py

@Author: Ogunmokun Olukunle

"""

from singaempire.models import *
from sqlalchemy import or_, and_
from singaempire.services import *
from singaempire import redis

BaseImageService = ServiceFactory.create_service(Image, db)
ArtistService = ServiceFactory.create_service(Artist, db)
AlbumService = ServiceFactory.create_service(Album, db)
BaseSongService = ServiceFactory.create_service(Song, db)
GenreService = ServiceFactory.create_service(Genre, db)
ContentTypeService = ServiceFactory.create_service(ContentType, db)
BaseGalleryService = ServiceFactory.create_service(Gallery, db)
BaseAdminMessageService = ServiceFactory.create_service(AdminMessage, db)
BaseAdminMessageResponseService = ServiceFactory.create_service(AdminMessageResponse, db)


class ImageService(BaseImageService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        if kwargs.get("filepath") is None:
            raise Exception('You have to upload at least one image')

        image = kwargs.get("filepath")
        upload_result = upload(image, public_id="SingaEmpire" + str(datetime.today()))
        os.remove(image)
        kwargs["url"] = upload_result["url"]
        obj = BaseImageService.create(**kwargs)

        return obj


class AdminMessageService(BaseAdminMessageService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseAdminMessageService.create(**kwargs)
        contact_message.send(obj.id)
        redis.zadd('recent_message', obj.id, json.dumps(obj.as_dict()))
        return obj


class AdminMessageResponse(BaseAdminMessageResponseService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseAdminMessageResponseService.create(**kwargs)
        contact_message_replied.send(obj.id)
        return obj

class GalleryService(BaseGalleryService):
    @classmethod
    def create(cls, ignored_args=None, **kwargs):
        obj = BaseGalleryService.create(**kwargs)
        return obj
    @classmethod
    def upload_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)

        image = kwargs.get("filepath")
        upload_result = upload(image, public_id="SingaEmpire" + str(datetime.today()))
        os.remove(image)
        kwargs["url"] = upload_result["url"]
        kwargs["gallery_id"] = obj.id
        image = BaseImageService.create(**kwargs)

        obj = BaseGalleryService.update(obj_id, image_id=image.id)
        return obj


class SongService(BaseSongService):
    @classmethod
    def upload_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)

        image = kwargs.get("filepath")
        upload_result = upload(image, public_id="SingaEmpire" + str(datetime.today()))
        os.remove(image)
        kwargs["url"] = upload_result["url"]
        kwargs["song_id"] = obj.id
        image = BaseImageService.create(**kwargs)

        obj = BaseSongService.update(obj_id, cover_image_id=image.id)
        return obj


