__author__ = 'kunsam002'

"""
restful.py

@Author: Ogunmokun Olukunle

"""

from singaempire.models import *
from sqlalchemy import or_, and_
from singaempire.services import *

NewsService = ServiceFactory.create_service(News, db)
BaseEventService = ServiceFactory.create_service(Event, db)
BasePostService = ServiceFactory.create_service(Post, db)
BaseImageService = ServiceFactory.create_service(Image, db)


class EventService(BaseEventService):
    @classmethod
    def upload_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)

        image = kwargs.get("filepath")
        upload_result = upload(image, public_id="SingaEmpire" + str(datetime.today()))
        os.remove(image)
        kwargs["url"] = upload_result["url"]
        kwargs["event_id"] = obj.id
        image = BaseImageService.create(**kwargs)

        obj = BaseEventService.update(obj_id, cover_image_id=image.id)
        return obj


class PostService(BasePostService):
    @classmethod
    def upload_image(cls, obj_id, **kwargs):
        obj = cls.get(obj_id)

        image = kwargs.get("filepath")
        upload_result = upload(image, public_id="SingaEmpire" + str(datetime.today()))
        os.remove(image)
        kwargs["url"] = upload_result["url"]
        kwargs["post_id"] = obj.id
        image = BaseImageService.create(**kwargs)

        obj = BaseEventService.update(obj_id, cover_image_id=image.id)
        return obj


