"""
site_services.py
@Author: Olukunle Ogunmokun
@Date: June 8, 2015

This module is for querying and sourcing all required data and information
needed by individual blueprints as an aid to speed optimization.

"""

from singaempire.models import *
from sqlalchemy.exc import IntegrityError
from singaempire import db, app, logger, cache, redis, models
from singaempire.core.utils import clean_kwargs, populate_obj, id_generator, slugify
from itertools import groupby
import json



def fetch_states():
    states = models.State.query.filter(models.State.country_code=="NG").order_by(models.State.name).all()

    results = [i.as_dict() for i in states]

    return results

def fetch_countries():
    countries = models.Country.query.filter(models.Country.code=="NG").all()

    results = [i.as_dict() for i in countries]

    return results

def fetch_artists():
    artists = models.Artist.query.all()

    results = [i.as_dict(extras=["state","country","total_songs","total_albums","cover_image_url"]) for i in artists]

    return results

def fetch_albums():
    albums = models.Album.query.all()

    results = [i.as_dict(extras=["total_songs"]) for i in albums]

    return results

def fetch_songs():
    songs = models.Song.query.all()

    results = [i.as_dict(extras=["artist","cover_image_url"]) for i in songs]

    return results

def fetch_news():
    news = models.News.query.all()

    results = [i.as_dict(extras=["user_id","user"]) for i in news]

    return results

def fetch_recent_messages():
    messages = models.AdminMessage.query.filter(AdminMessage.is_replied==False).all()

    results = [i.as_dict() for i in messages]

    return results
