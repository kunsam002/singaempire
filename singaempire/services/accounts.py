__author__ = 'kunsam002'

"""
restful.py

@Author: Ogunmokun Olukunle

"""

from singaempire.models import *
from sqlalchemy import or_, and_
from singaempire.services import *

def authenticate_user(username, password, **kwargs):
    """
    Fetch a user based on the given username and password.

    :param username: the username (or email address) of the user
    :param password: password credential
    :param kwargs: additional parameters required

    :returns: a user object or None
    """
    # user_login_attempted.send(username)

    user = User.query.filter(or_(User.username == username,
                                 User.email == username)).first()
    if user and user.check_password(password):
        return user
    return None

def authenticate_forgot_password(username, **kwargs):
    """
    """
    user = User.query.filter(
        and_(User.email is not None)).filter(
        or_(User.username == username, User.email == username)).first()
    if user:
        return user
    else:
        return None


BaseUserService = ServiceFactory.create_service(User, db)


class UserService(BaseUserService):
    @classmethod
    def create(cls, ignored_args=None, include_address=True, **kwargs):
        """ Overriding the parent class method to create a service which registers users """

        password = kwargs.pop("password", None)

        user = BaseUserService.create(**kwargs)
        user.set_password(password)

        return user

    @classmethod
    def update(cls, obj_id, ignored_args=None, **kwargs):
        ignored_args = ["id", "date_created", "email", "username", "password", "last_updated", "username"]

        user = BaseUserService.get(obj_id)

        user = BaseUserService.update(user.id, ignored_args=ignored_args, **kwargs)

        # user account update notification
        message_data = {'user': user}
        return user
