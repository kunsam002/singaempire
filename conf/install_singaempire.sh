#!/bin/sh
# @Author: kunsam002
# @Date:   2017-10-01
# @Last Modified by:   Olukunle Ogunmokun
# @Last Modified time: 2017-10-01

# Create application account
sudo useradd -M -s /bin/bash -U singaempire
sudo addgroup singaempire

sudo chgrp singaempire /opt/singaempire
sudo chmod g+s /opt/singaempire

sudo find /opt/singaempire -type d -exec chgrp singaempire {} +
sudo find /opt/singaempire -type d -exec chmod g+s {} +


# copy uwsgi.conf to upstart
sudo cp uwsgi.conf /etc/init/
sudo ln -s /lib/init/upstart-job /etc/init.d/uwsgi
#sudo ln -s /opt/singaempire/conf/celery.service /etc/systemd/system/celery.service

# copy celery.conf to upstart
sudo cp celery.conf /etc/init/
sudo ln -s /lib/init/upstart-job /etc/init.d/celery

# create necessary uwsgi folders
sudo mkdir -p /var/log/uwsgi
sudo mkdir -p /etc/uwsgi/apps-enabled
sudo mkdir -p /etc/uwsgi/apps-available
sudo mkdir -p /var/log/celery
#sudo mkdir -p /var/run/celery

# necessary log folder for logging
sudo mkdir -p /var/log/singaempire
sudo mkdir -p /var/log/nginx/singaempire
sudo chown -R singaempire.singaempire /var/log/singaempire
sudo chown -R singaempire.singaempire /var/log/nginx
sudo chown -R singaempire.singaempire /var/log/celery

#sudo chown -R singaempire.singaempire /var/log/celery
#sudo chown -R singaempire.singaempire /var/run/celery

# create necessary symbolic links for uwsgi
sudo ln -s /opt/singaempire/conf/uwsgi/testmain.ini /etc/uwsgi/apps-enabled
sudo ln -s /opt/singaempire/conf/uwsgi/testadmin.ini /etc/uwsgi/apps-enabled

# create necessary symbolic links for nginx
sudo ln -s /opt/singaempire/conf/nginx/singaempire.com /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default


# Restart nginx server
#sudo service uwsgi restart
sudo service nginx restart
sudo service celery restart
#sudo systemctl restart celery.service
