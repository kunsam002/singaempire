upstream web_cluster {

  #least_conn;
  server  138.197.9.14;
}


server {
	listen 80;
    server_name admin.singaempire.com;

  location ~ ^/(img|js|css|fonts)/ {  # |pi||ext|theme
    root                    /opt/singaempire/singaempire/static;
    add_header              Cache-Control public;
    expires                 30d;
    #access_log              off;
    access_log                            /var/log/nginx/singaempire/admin.access.log;
    error_log                             /var/log/nginx/singaempire/admin.error.log;
  }

  location / {
   proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   Host      $http_host;

        # these two lines here
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_send_timeout   600;
      proxy_read_timeout   600;
        proxy_pass http://localhost:5550;
        }

  location /static {
    alias                    /opt/singaempire/singaempire/static;
    add_header              Cache-Control public;
    expires                 30d;
  }

  location /robots.txt {
		alias                    /opt/singaempire/singaempire/robots.txt;
		add_header              Cache-Control public;
		expires                 30d;
	}

  location /favicon.ico {
    deny        all;
  }

  location @yourapplication {
    include uwsgi_params;
    uwsgi_pass unix:///tmp/admin.sock;
  }

}

server {
        listen 80;
        server_name www.singaempire.com m.singaempire.com singaempire.com;

  location ~ ^/(img|js|css|fonts)/ {  # |pi||ext|theme
        root                    /opt/singaempire/singaempire/static;
        add_header              Cache-Control public;
        expires                 30d;
        #access_log              off;
        access_log                            /var/log/nginx/singaempire/main.access.log;
        error_log                             /var/log/nginx/singaempire/main.error.log;
  }

  location / {
   proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   Host      $http_host;

        # these two lines here
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        proxy_send_timeout   600;
      proxy_read_timeout   600;
        proxy_pass http://localhost:5500;
   }

  location /static {
    alias                    /opt/singaempire/singaempire/static;
    add_header              Cache-Control public;
    expires                 30d;
  }

  location /robots.txt {
		alias                    /opt/singaempire/singaempire/robots.txt;
		add_header              Cache-Control public;
		expires                 30d;
	}

  location /favicon.ico {
    deny        all;
  }

  location @yourapplication {
    include uwsgi_params;
    uwsgi_pass unix:///tmp/main.sock;
  }


}



