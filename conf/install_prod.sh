#!/bin/sh
# @Author: kunsam002
# @Date:   2017-10-01
# @Last Modified by:   Olukunle Ogunmokun
# @Last Modified time: 2017-10-01

# Create application account
sudo useradd -M -s /bin/bash -U singaempire
sudo addgroup singaempire

sudo chgrp singaempire /opt/singaempire
sudo chmod g+s /opt/singaempire

sudo find /opt/singaempire -type d -exec chgrp singaempire {} +
sudo find /opt/singaempire -type d -exec chmod g+s {} +


# setup uwsgi service
sudo ln -s /opt/singaempire/conf/uwsgi.service /etc/systemd/system/uwsgi.service
sudo ln -s /opt/singaempire/conf/celery.service /etc/systemd/system/celery.service

# necessary log folder for logging
sudo mkdir -p /var/log/singaempire
sudo mkdir -p /var/log/nginx/singaempire
sudo mkdir -p /var/log/uwsgi

sudo mkdir -p /var/log/celery
sudo mkdir -p /var/run/celery

# set necessary permissions
sudo chown -R singaempire.singaempire /var/log/singaempire
sudo chown -R singaempire.singaempire /var/log/nginx
sudo chown -R singaempire.singaempire /var/log/uwsgi

sudo chown -R singaempire.singaempire /var/log/celery
sudo chown -R singaempire.singaempire /var/run/celery

# create necessary symbolic links for nginx
sudo ln -s /opt/singaempire/conf/nginx/singaempire.com /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default
sudo mkdir -p /etc/uwsgi
sudo ln -s /opt/singaempire/conf/uwsgi/*.ini /etc/uwsgi/
## create necessary symbolic links for uwsgi
#sudo ln -s /opt/singaempire/conf/uwsgi/main.ini /etc/uwsgi/apps-enabled
#sudo ln -s /opt/singaempire/conf/uwsgi/admin.ini /etc/uwsgi/apps-enabled

# Restart nginx server
sudo systemctl daemon-reload
sudo systemctl restart uwsgi.service
sudo systemctl restart celery.service
sudo systemctl restart nginx.service

