#! /usr/bin/env python

from flask.ext.script import Manager
import os
import flask
from flask.ext.assets import ManageAssets
from factories import create_app, initialize_api, initialize_blueprints
from flask.ext.migrate import MigrateCommand
from model_migrations import *
from xlrd import open_workbook
from multiprocessing import Process

app = create_app('singaempire', 'config.LiveConfig')

logger = app.logger

# Initializing script manager
manager = Manager(app)
# add assets command to it
manager.add_command("assets", ManageAssets(app.assets))

manager.add_command('db', MigrateCommand)

SETUP_DIR = app.config.get("SETUP_DIR")


@manager.command
def runserver():
    """ Start the server"""
    # with app2.app_context():
    from singaempire.views.public import www
    from singaempire import api, principal
    from singaempire.resources import resource

    from singaempire import redis_worker

    from flask_debugtoolbar import DebugToolbarExtension
    from flask_debugtoolbar_lineprofilerpanel.profile import line_profile

    app.config['DEBUG_TB_PANELS'] = [
        'flask_debugtoolbar.panels.versions.VersionDebugPanel',
        'flask_debugtoolbar.panels.timer.TimerDebugPanel',
        'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
        'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask_debugtoolbar.panels.template.TemplateDebugPanel',
        'flask_debugtoolbar.panels.sqlalchemy.SQLAlchemyDebugPanel',
        'flask_debugtoolbar.panels.logger.LoggingPanel',
        'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
        # Add the line profiling
        'flask_debugtoolbar_lineprofilerpanel.panels.LineProfilerPanel'
    ]

    toolbar = DebugToolbarExtension(app)

    # Initialize the app blueprints
    initialize_blueprints(app, www)
    initialize_api(app, api)

    port = int(os.environ.get('PORT', 5500))
    app.run(host='0.0.0.0', port=port)


@manager.command
def runadmin():
    """ Start the backend server server"""
    # with app2.app_context():
    from singaempire.views.admin import control
    # Initialize the app blueprints
    initialize_blueprints(app, control)

    port = int(os.environ.get('PORT', 5550))
    app.run(host='0.0.0.0', port=port)

@manager.command
def runserver_prod():
    """ Start the server"""
    # with app2.app_context():
    from singaempire.views.public import www
    from singaempire import api, principal
    from singaempire.resources import resource

    from singaempire import redis_worker

    # Initialize the app blueprints
    initialize_blueprints(app, www)
    initialize_api(app, api)

    port = int(os.environ.get('PORT', 5500))
    app.run(host='0.0.0.0', port=port)


@manager.command
def runadmin_prod():
    """ Start the backend server server"""
    # with app2.app_context():
    from singaempire.views.admin import control
    # Initialize the app blueprints
    initialize_blueprints(app, control)

    port = int(os.environ.get('PORT', 5550))
    app.run(host='0.0.0.0', port=port)


@manager.command
def syncdb(refresh=False):
    """
    Synchronizes (or initializes) the database
    :param refresh: drop and recreate the database
    """
    # Apparently, we need to import the models file before this can work right.. smh @flask-sqlalchemy
    from singaempire import db, models
    if refresh:
        logger.info("Dropping database tables")
        db.drop_all()
    logger.info("Creating database tables")
    db.create_all()
    db.session.flush()


@manager.command
def alembic(action, message=""):
    """ alembic integration using Flask-Alembic. Should provide us with more control over migrations """

    app = flask.current_app
    with app.app_context():
        from singaempire import alembic as _alembic
        from singaempire.models import *

        if action == "migrate":
            app.logger.info("Generating migration")
            _alembic.revision(message)
            app.logger.info("Migration complete")

        elif action == "upgrade":
            app.logger.info("Executing upgrade")
            _alembic.upgrade()
            app.logger.info("Upgrade complete")

        elif action == 'update':
            app.logger.info("Executing upgrade")
            _alembic.upgrade()
            _alembic.revision("Generating migration")
            _alembic.upgrade()
            app.logger.info("Upgrade complete")


@manager.command
def install_assets(name):
    """ load startup data for a particular module """

    app = flask.current_app
    with app.app_context():
        from singaempire import db, models, logger
        from utilities import loader

        setup_dir = app.config.get("SETUP_DIR")  # Should be present in config

        filename = "%s.json" % name

        src = os.path.join(setup_dir, filename)
        logger.info(src)

        loader.load_data(models, db, src)



@manager.command
def setup_app():
    syncdb()
    install_assets()


def runInParallel(*fns):
    proc = []
    for fn in fns:
        p = Process(target=fn)
        p.start()
        proc.append(p)
    for p in proc:
        p.join()


@manager.command
def start_apps():
    runInParallel(runserver_prod, runadmin_prod)



if __name__ == "__main__":
    manager.run()
