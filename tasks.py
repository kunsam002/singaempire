from factories import create_app
import socket

# config_obj = 'config.StagingConfig'
# switch to live configuration based on system name
config_obj = 'config.ProdConfig'

# Use partials to include the app_name parameter in the product create_app function
app = create_app('buyorbid', config_obj)

with app.app_context():
   from singaempire.async import *
   from singaempire.async import *
   # from singaempire.services.product import *
   # from singaempire.subscribers.search import *
   # from buyorbid.services.admin import *
   from singaempire.models import *

   celery_app = app.celery
