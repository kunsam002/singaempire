import os
import uwsgi
import gevent.monkey

gevent.monkey.patch_all()

from factories import create_app, initialize_api, initialize_blueprints

app = create_app('singaempire', 'config.SiteDevConfig')

with app.app_context():
    from singaempire.views.admin import control
    from singaempire import api

    # Import all subscriptions to initialize them
    initialize_api(app, api)

    # Initialize the app blueprints
    initialize_blueprints(app, control)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
